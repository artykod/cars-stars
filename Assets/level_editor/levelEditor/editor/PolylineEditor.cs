﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Polyline))]
public class PolylineEditor : Editor {
	
	private void OnEnable() {
		SceneView.onSceneGUIDelegate += OnSceneGUIDraw;
	}

	private void OnDisable() {
		SceneView.onSceneGUIDelegate -= OnSceneGUIDraw;
	}

	public override void OnInspectorGUI() {
		GUILayout.Label(
@"
README:

Все действия выполняются в окне Scene

Ctrl+ЛКМ - добавляет точку в конец линии
Shift+ЛКМ - добавляет точку в порядке к ближайшей найденной точке 
(т.о. можно повышать качество линии в нужных местах)

Alt+ЛКМ - удаляет точку под курсором
");

		base.OnInspectorGUI();
	}

	private void OnSceneGUIDraw(SceneView sceneView) {
		Polyline line = target as Polyline;

		if (line == null) {
			return;
		}
		
		bool needRebuild = false;

		// при нажатиях добавляются/удаляются контрольные точки
		if (Event.current.type == EventType.MouseDown) {
			Camera camera = SceneView.lastActiveSceneView.camera;
			Vector3 mpos = Event.current.mousePosition;
			Vector3 pos = camera.ScreenToWorldPoint(mpos);
			pos.z = 0f;
			float dif = camera.transform.position.y - pos.y;
			pos.y = camera.transform.position.y + dif;

			// поиск ближайшей точки к курсору мыши
			float minDist = float.MaxValue;
			int index = -1;
			for (int i = 0; i < line.Dots.Count; i++) {
				Vector2 dot = line.Dots[i];

				float dist = (pos - new Vector3(dot.x, dot.y)).magnitude;

				if (dist < minDist) {
					index = i;
					minDist = dist;
				}
			}

			if (Event.current.shift) { // при зажатом ctrl добавляется точка

				if (index >= 0) { // добавление между точками, исходя из ближайшей к курсору мыши
					line.Dots.Insert(index + 1, new Vector2(pos.x, pos.y));
				} else {
					line.Dots.Add(new Vector2(pos.x, pos.y));
				}

			} else if (Event.current.control) {

				line.Dots.Add(new Vector2(pos.x, pos.y));

			} if (Event.current.alt) { // при зажатом альте удаляется ближайшая к мышке точка

				if (index >= 0) {
					line.Dots.RemoveAt(index);
				}

			}

			needRebuild = true;
		}

		// отрисовка контролов для точек
		for (int i = 0; i < line.Dots.Count; i++) {
			Vector2 dot = line.Dots[i];
			line.Dots[i] = Handles.PositionHandle(dot, Quaternion.identity);
			Handles.Label(line.Dots[i], "p: " + i);

			if (dot.x != line.Dots[i].x || dot.y != line.Dots[i].y) {
				needRebuild = true;
			}
		}

		if (needRebuild) {
			EditorUtility.SetDirty(target);
		}

		DrawLine(line);

		// блокировка нажатий в окне сцены пока выделен объект
		if (Selection.activeObject == line.gameObject) {
			HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
		} else {
			HandleUtility.Repaint();
		}

		if (GUI.changed) {
			EditorUtility.SetDirty(target);
		}
	}

	public static void DrawLine(Polyline line) {
		if (line != null && line.Dots.Count > 1) {
			Vector3 a, b;
			a = line.Dots[0];
			for (int i = 1; i < line.Dots.Count; i++) {
				b = line.Dots[i];
				Handles.color = Color.yellow;
				Handles.DrawLine(a, b);
				a = b;
			}
		}
	}
}
