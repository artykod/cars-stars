﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Level))]
public class LevelEditor : Editor {

	[MenuItem("Level editor/Create level in scene")] 
	private static void CreateLevel() {
		GameObject level = new GameObject("level");
		level.AddComponent<Level>();
		Selection.activeGameObject = level;
	}

	private void OnEnable() {
		SceneView.onSceneGUIDelegate += OnSceneGUIDraw;
	}

	private void OnDisable() {
		SceneView.onSceneGUIDelegate -= OnSceneGUIDraw;
	}

	public override void OnInspectorGUI() {
		GUILayout.Label(
@"
README:

В окне Scene рисуются все имеющиеся в чилдах линии

Новая линия добавляется нажатием на кнопку Add polyline

Новый уровень можно создать из меню Unity:
	Level editor -> Create level in scene

Префаб уровня нужно сохранить в папку:
	resources/levels/prefabs
там же рядом лежат соответствующие уровню картинки:
	resources/levels/cars
	resources/levels/backgrounds
имена картинок оттуда указываются тут же в настройках:

");

		base.OnInspectorGUI();

		Level level = target as Level;

		if (level == null) {
			return;
		}

		if (GUILayout.Button("Add polyline")) {
			GameObject line = new GameObject("line");
			line.AddComponent<Polyline>();
			line.transform.SetParent(level.transform);
			Selection.activeGameObject = line;
		}
	}

	private void OnSceneGUIDraw(SceneView sceneView) {
		Level level = target as Level;

		if (level == null) {
			return;
		}

		Polyline[] lines = level.gameObject.GetComponentsInChildren<Polyline>();
		foreach (var i in lines) {
			PolylineEditor.DrawLine(i);
		}
	}
}