﻿using UnityEngine;

public class Level : MonoBehaviour {
	[SerializeField]
	private string carImageName = "car_1";
	[SerializeField]
	private string backgroundImageName = "background_1";
	[SerializeField]
	private Color titleColor = Color.white;
	[SerializeField]
	private string titleCarName = "";
	[SerializeField]
	private int order = 0;

	public string CarImageName {
		get {
			return carImageName;
		}
	}
	public string BackgroundImageName {
		get {
			return backgroundImageName;
		}
	}
	public Color TitleColor {
		get {
			return titleColor;
		}
	}
	public string TitleCarName {
		get {
			return titleCarName;
		}
	}
	public int Order {
		get {
			return order;
		}
	}

	public Texture2D LoadCarImage() {
		return Resources.Load<Texture2D>("levels/cars/" + carImageName);
	}

	public Texture2D LoadBackgroundImage() {
		return Resources.Load<Texture2D>("levels/backgrounds/" + backgroundImageName);
	}

	public Polyline[] Polylines() {
		return gameObject.GetComponentsInChildren<Polyline>();
	}
}
