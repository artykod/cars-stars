﻿using UnityEngine;
using System.Collections.Generic;

public class Polyline : MonoBehaviour {
	[SerializeField]
	private List<Vector2> dots = new List<Vector2>();
	
	public List<Vector2> Dots {
		get {
			return dots;
		}
	}
}
