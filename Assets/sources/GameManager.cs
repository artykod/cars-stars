﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	public enum ButtonType {
		Left = 0,
		Right = 1,
	}

	[SerializeField]
	private LevelBuilder levelBuilderPrefab = null;
	[SerializeField]
	private AnimationCurve normalizeRotationAnimationCurve = null;
	[SerializeField]
	private CanvasGroup endLevelUI = null;
	[SerializeField]
	private CanvasGroup startScreenGroup = null;
	[SerializeField]
	private Button leftButton = null;
	[SerializeField]
	private Button rightButton = null;
	[SerializeField]
	private Text carTitle = null;
	[SerializeField]
	private Text winText = null;

	private LevelBuilder levelBuilder = null;
	private Vector3 oldPosition = Vector3.zero;
	private bool inited = false;

	private bool canControls = false;
	private bool gameStarted = false;

	private static bool startSceenShowed = false;

	private static int currentLevel = 0;

	private static bool gameInitalized = false;

	private bool IsGameEnd {
		get {
			return levelBuilder != null ? currentLevel >= levelBuilder.LevelsCount : false;
		}
	}

	private void SetMaterialTintColor(Material m, Color c) {
		m.SetColor("_Color", c);
	}
	private void SetMaterialTintAlpha(Material m, float alpha) {
		m.SetColor("_Color", new Color(1f, 1f, 1f, alpha));
	}
	private void SetCanvasGroupTintAlpha(CanvasGroup group, float alpha) {
		group.alpha = alpha;

	}

	public void ClickedButton(int button) {
		if (canControls) {
			return;
		}
		
		bool isGameEnd = IsGameEnd;

		switch ((ButtonType)button) {
		case ButtonType.Left:
			if (isGameEnd) {
				currentLevel = 0;
				Application.LoadLevel("main");
			} else {
				Application.LoadLevel("main");
			}
			break;
		}
	}

	private void Awake() {
		if (!gameInitalized) {
			DebugConsole.Initialize();
			Config.Load();

#if !UNITY_IPHONE && !UNITY_ANDROID
			Debug.LogFormat("Set screen resolution: {0}x{1} fullscreen: {2}", Config.ScreenWidth, Config.ScreenHeight, Config.Fullscreen);
			Screen.SetResolution(Config.ScreenWidth, Config.ScreenHeight, Config.Fullscreen, 60);
#endif
			Application.targetFrameRate = 60;

			gameInitalized = true;
		}

		Input.multiTouchEnabled = false;

		levelBuilder = Instantiate(levelBuilderPrefab, new Vector3(0f, 0f), Quaternion.identity) as LevelBuilder;

		SetMaterialTintAlpha(levelBuilder.CarMaterial, 0f);
		endLevelUI.alpha = 0f;
		startScreenGroup.alpha = 0f;

		levelBuilder.LoadLevel(currentLevel);

		Random.seed = System.DateTime.UtcNow.Millisecond;
		levelBuilder.transform.rotation = Quaternion.Euler(Random.Range(90f, 360f), Random.Range(90f, 360f), Random.Range(90f, 360f));

		SetMaterialTintAlpha(levelBuilder.StarsMaterial, 1f);

		carTitle.text = levelBuilder.CurrentCarTitle;
		carTitle.color = levelBuilder.CurrentCarTitleColor;

		winText.gameObject.SetActive(false);

		if (!startSceenShowed) {
			SetCanvasGroupTintAlpha(startScreenGroup, 1f);
			gameStarted = false;
			canControls = true;

			StartCoroutine(StartScreenAnimation());

			startSceenShowed = true;
		} else {
			gameStarted = true;
			canControls = true;
		}
	}

	private IEnumerator StartScreenAnimation() {

		yield return new WaitForSeconds(Config.StartGameTimer);

		float time = 1f;
		float currentTime = 0f;
		while (currentTime <= time) {
			currentTime += Time.deltaTime;
			float t = Mathf.Min(1f, currentTime / time);

			SetCanvasGroupTintAlpha(startScreenGroup, 1f - t);

			yield return null;
		}

		gameStarted = true;
	}

	private void Update() {
		if (!inited || Input.GetMouseButtonDown(0)) {
			inited = true;
			oldPosition = Input.mousePosition;
		}

		Vector3 delta = (Input.mousePosition - oldPosition) * Config.Sensitivity;
		oldPosition = Input.mousePosition;
		
		if (gameStarted && canControls && Input.GetMouseButton(0)) {
			levelBuilder.transform.RotateAround(levelBuilder.transform.position, new Vector3(0f, 1f), -delta.x);
			levelBuilder.transform.RotateAround(levelBuilder.transform.position, new Vector3(1f, 0f), delta.y);
		}

		Vector3 angles = levelBuilder.transform.rotation.eulerAngles;
		Vector3 dif = new Vector3(Mathf.Abs(angles.x - 360f), Mathf.Abs(angles.y - 360f), Mathf.Abs(angles.z - 360f));
		float limit = 10f;

		float difference = Mathf.Abs(Quaternion.Dot(levelBuilder.transform.rotation, Quaternion.identity));
		float alpha = Mathf.Max(0.35f, Mathf.Pow(difference, 25f));

		if ((dif.x <= limit || angles.x <= limit) && (dif.y <= limit || angles.y <= limit) && (dif.z <= limit || angles.z <= limit)) {
			if (gameStarted) {
				if (canControls) {
					StartCoroutine(NormalizeLevelRotation(alpha));
				}
				canControls = false;
			}
		} else {
			SetMaterialTintAlpha(levelBuilder.LinesMaterial, alpha);
		}

		levelBuilder.TransformObjectPositions(levelBuilder.transform);
	}

	private IEnumerator NormalizeLevelRotation(float startLinesAlpha) {
		float time = 1f;
		float currentTime = 0f;
		Quaternion originalRotation = levelBuilder.transform.rotation;

		while (currentTime <= time) {
			currentTime += Time.deltaTime * 1.5f;
			float t = Mathf.Min(1f, currentTime / time);

			if (normalizeRotationAnimationCurve != null) {
				t = normalizeRotationAnimationCurve.Evaluate(t);
			}

			levelBuilder.transform.rotation = Quaternion.Lerp(originalRotation, Quaternion.identity, t);
			SetMaterialTintAlpha(levelBuilder.LinesMaterial, Mathf.Lerp(startLinesAlpha, 1f, t));

			yield return null;
		}

		currentLevel++;

		if (IsGameEnd) {

			winText.gameObject.SetActive(true);
			winText.color = levelBuilder.CurrentCarTitleColor;

			leftButton.GetComponentInChildren<Text>().text = "ПЕРЕИГРАТЬ";
		} else {
			leftButton.GetComponentInChildren<Text>().text = "СЛЕДУЮЩИЙ УРОВЕНЬ";
		}

		time = 1f;
		currentTime = 0f;

		while (currentTime <= time) {
			currentTime += Time.deltaTime * 0.5f;
			float t = Mathf.Min(1f, currentTime / time);

			SetMaterialTintAlpha(levelBuilder.CarMaterial, t);
			endLevelUI.alpha = t;
			SetMaterialTintAlpha(levelBuilder.LinesMaterial, 1f - t);
			SetMaterialTintAlpha(levelBuilder.StarsMaterial, 1f - t);

			yield return null;
		}

		yield return new WaitForSeconds(1f);
	}
}
