﻿using UnityEngine;
using System.Collections.Generic;

public class Levels {
	private const float PIXELS_PER_UNIT = 100f;
	private const float RANDOM_Z_MIN = -500f;
	private const float RANDOM_Z_MAX =  500f;

	private Vector3[][] points = new Vector3[][] {
		new Vector3[] {
			new Vector3(-662f,  258f, randomDepth),
			new Vector3(-290f,  324f, randomDepth),
			new Vector3(-452f,  260f, randomDepth),
			new Vector3(-333f,  255f, randomDepth),
			new Vector3(-752f,  177f, randomDepth),
			new Vector3(-732f,  134f, randomDepth),
			new Vector3(-374f,  168f, randomDepth),
			new Vector3(-183f,  156f, randomDepth),
			new Vector3(-532f,  105f, randomDepth),
			new Vector3(-744f,   -6f, randomDepth),
			new Vector3(-616f,   -4f, randomDepth),
			new Vector3(-550f,   18f, randomDepth),
			new Vector3(-342f,   60f, randomDepth),
			new Vector3(-151f,    9f, randomDepth),
			new Vector3(-360f,  -22f, randomDepth),
			new Vector3(-602f,  -68f, randomDepth),
			new Vector3(-716f,  -90f, randomDepth),
			new Vector3(-179f,  -85f, randomDepth),
			new Vector3(-529f, -146f, randomDepth),
			new Vector3(-650f, -148f, randomDepth),
			new Vector3(-314f, -232f, randomDepth),
			new Vector3( -52f,  342f, randomDepth),
			new Vector3(  51f,  402f, randomDepth),
			new Vector3( 204f,  344f, randomDepth),
			new Vector3( -34f,  230f, randomDepth),
			new Vector3(  38f,  169f, randomDepth),
			new Vector3( 129f,  210f, randomDepth),
			new Vector3( 286f,  244f, randomDepth),
			new Vector3( 306f,  203f, randomDepth),
			new Vector3( 266f,  139f, randomDepth),
			new Vector3( 150f,   41f, randomDepth),
			new Vector3( 464f,  188f, randomDepth),
			new Vector3( 546f,  132f, randomDepth),
			new Vector3( 684f,  123f, randomDepth),
			new Vector3( 725f,  198f, randomDepth),
			new Vector3(   0f,  -17f, randomDepth),
			new Vector3( -25f, -100f, randomDepth),
			new Vector3( 218f,  -20f, randomDepth),
			new Vector3( 568f,  -10f, randomDepth),
			new Vector3( 322f, -115f, randomDepth),
			new Vector3( 212f, -160f, randomDepth),
			new Vector3( 773f,   36f, randomDepth),
			new Vector3( -40f, -230f, randomDepth),
			new Vector3( 226f, -221f, randomDepth),
			new Vector3( 338f, -263f, randomDepth),
			new Vector3( 477f, -175f, randomDepth),
			new Vector3( 545f, -197f, randomDepth),
			new Vector3( 758f, -136f, randomDepth),
			new Vector3( 647f, -290f, randomDepth),
			new Vector3( 424f, -376f, randomDepth),
			new Vector3( 368f, -360f, randomDepth),
			new Vector3( -42f, -354f, randomDepth),
		},
	};

	private Vector3[][][] levels = null;
	private List<Level> levelsInfo = new List<Level>(5);

	private static float randomDepth {
		get {
			return Random.Range(RANDOM_Z_MIN, RANDOM_Z_MAX);
		}
	}

	private static float getZ(Vector3[] stars, float x, float y) {
		int pIndex = -1;
		float minDist = float.MaxValue;
		float dist = 0f;

		for (int i = 0; i < stars.Length; i++) {
			dist = new Vector2(x - stars[i].x, y - stars[i].y).magnitude;

			if (dist < minDist) {
				minDist = dist;
				pIndex = i;
			}
		}

		return stars[pIndex].z / PIXELS_PER_UNIT;
	}

	public int GetLevelsCount() {
		return levels.Length;
	}

	public Vector3[][] GetLevel(int index) {
		return levels[index];
	}

	public Level GetLevelInfo(int index) {
		return levelsInfo[index];
    }

	public Levels() {
		Level[] levelsInResources = Resources.LoadAll<Level>("levels/prefabs");
		List<Vector3[][]> levels = new List<Vector3[][]>(levelsInResources.Length);
		List<Level> levelsInstances = new List<Level>(levelsInResources.Length);

		for (int i = 0; i < levelsInResources.Length; i++) {
			levelsInstances.Add(GameObject.Instantiate(levelsInResources[i]));
		}

		levelsInstances.Sort((level1, level2) => {
			return level1.Order >= level2.Order ? 1 : -1;
		});

		for (int i = 0; i < levelsInstances.Count; i++) {
			Level level = levelsInstances[i];
			Polyline[] polylines = level.Polylines();
			List<Vector3[]> lines = new List<Vector3[]>(polylines.Length);

			levelsInfo.Add(level);

			for (int j = 0; j < polylines.Length; j++) {
				Polyline polyline = polylines[j];
				List<Vector2> lineDots = polyline.Dots;
				List<Vector3> dots = new List<Vector3>(lineDots.Count);

				for (int d = 0; d < lineDots.Count; d++) {
					Vector2 dot = lineDots[d] * PIXELS_PER_UNIT;
					dot.y = -dot.y;
                    dots.Add(new Vector3(dot.x, dot.y, getZ(points[0], dot.x, dot.y)));
				}

				if (dots.Count > 1) {
					lines.Add(dots.ToArray());
				} else {
					Debug.LogWarningFormat("Polyline has less than 2 dots. Name: {0} Index: {1} in level: {2}", polyline.name, j, level.name);
				}
			}

			levels.Add(lines.ToArray());
		}

		this.levels = levels.ToArray();
	}
}
