Описание редактора уровней:

редактор уровней представляет из себя скрипты редактора Unity, с помощью которых можно 
создавать наборы линий и сохранять их в префабы, которые затем будут подгружаться игрой.

есть отдельная папка level_editor в Assets. внутри есть сцена редактора.
в сцене уже настроена камера как в игре и заготовка под текстуру машины: Car
у Car есть отдельный материал, которому нужно указать нужную текстуру машины,
по которой будет строиться уровень.

после выбора нужной текстуры можно начать строить уровень.
новый уровень создается через меню Unity: Level editor -> Create level in scene.
после этого в сцене появится префаб level. его нужно переименовать в нужное имя и сохранить
закинуть в папку resources/levels/prefabs (оттуда игра берет все имеющиеся уровни)

затем можно строить линии на уровне.
выбрав созданный уровень в сцене в окне Inspector будут его свойства.
ниже свойств есть кнопка Add polyline. она добавляет линию в уровень.
после добавления нужно выделить созданную линию в сцене (если это не произошло автоматом)

теперь в окне SceneView можно редактировать линию:
	Ctrl+ЛКМ - добавляет точку в порядке к ближайшей найденной точке (т.о. можно повышать качество линии в нужных местах)
	Alt+ЛКМ - удаляет точку под курсором
	с зажатым Shift можно перемещать всю линию
	

после построения одной линии нужно просто создать еще одну кнопкой Add polyline и т.д.

после построения всех необходимых линий нужно сохранить весь объект уровня в префаб, который закинули в папку уровней.
чтобы сохранить, нужно выделить объект уровня в сцене и в окне Inspector нажать на кнопку Apply.
после этого нажать Ctrl+S или сохранить сцену через меню юнити.
ПРИМЕЧАНИЕ: периодически сохраняя префаб, можно избежать повторения всей этой рутинной работы по настройке линий,
т.к. при каком-то сбое в юнити или еще каких-то проблемах в системе, все будет сохранено и не 
потеряется :-)

после сохранения уровня можно загрузить основную сцену игры scenes/main и поиграть в построенные уровни